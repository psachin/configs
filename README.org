* My personal configs

** Usage

   - Clone this repo:

     /WARNING/: This may overwrite your current config files. Make sure you
     backup existing files before you copy the configurations to =$HOME=.

     #+BEGIN_SRC shell
       git clone https://gitlab.com/psachin/configs.git
     #+END_SRC

   - Copy required configurations:
     #+BEGIN_SRC shell -n
       cd configs
       # Skip Vim & non configuration files
       rsync -v --exclude={".git/",".vim*rc","*.sh","*.org"} .git* .*rc ~/

       # Sample output:
       $ rsync -v --exclude={".git/",".vim*rc","*.sh","*.org"} .git* .*rc ~/
       .bashrc
       .git-completion.bash
       .gitconfig
       .gitconfig.local
       .gitignore_global
       .gitmessage.txt
       .gitmodules
       .muttrc

       sent 58,273 bytes  received 168 bytes  116,882.00 bytes/sec
       total size is 57,735  speedup is 0.99
     #+END_SRC

   - Make changes to configurations as required.

** License

   GNU GPL version 3.
