# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=


# ----------User specific customizations----------

# Color prompt(uncomment to activate)
#PS1='\[\e[0;34m\]\u\[\e[0m\]\[\e[0;35m\]@\[\e[0m\]\[\e[0;31m\]\h\[\e[0m\]\[\e[0;32m\]:\[\e[0m\]\[\e[01;30m\]\w\[\e[0m\]\[\e[0;32m\]$SIGN\[\e[0m\] '

# -------Command Alias-------------
alias lll='ls -al --color'
alias ll='ls -l --color'
alias srch='find . -type f -iname'
alias srchd='find . -type d -iname'
alias dh='df -h'

alias l='ls -lF --group-directories-first --color=always'
alias d='ls -F --group-directories-first --color=always'
alias la="ls -lAF --group-directories-first --color=always"
alias di='ls -d */'        # display only directories [Ref: commandlinefu.com ]
alias nol='ls -l --group-directories-first --color=none'    # nocolor to directories
alias emc='emacsclient -nw'
alias mnt='mount | column -t | grep -i /dev/[shl][dro] | sort'
alias zip='zip -9'         # will zip witg max compression [usage: zip <archives>.zip file(s)]
                           # example: zip pics.zip bella.png wallpaper.jpg
alias xtm='xterm -sl 2000 -bg black -fg green -e $SHELL -l'
alias rm='rm -iv'
alias ej='eject'
alias cl='eject -t'
alias da='date'
alias src='source'
alias ec='ej; sleep 3; cl'
alias ..='cd ..'
alias cp='cp -v'
alias mv='mv -v'
# --------------------

# Create and change directory
function mcd()
{
    if [ "$#" -eq 1 ]
    then
        mkdir --parents "$1"
        cd "$1"
    else
        echo "Usage: mcd <DIR_NAME>" > /dev/stderr
    fi
}
# ====================

# export PAGER="most"		# man pages in color (filmbykrish)

# ====================
source ~/.git-completion.bash

# export CC="arm-linux-gnueabi-gcc"

# export android port
export AP_PORT=9999

# Disable gnome-ssh-askpass
unset SSH_ASKPASS

# ---------Personal-----------
# git clone https://gitlab.com/psachin/bash_scripts ~/source/bash_scripts
# gituser()
#source ~/source/bash_scripts/git_switch_accounts.sh

# mplayer
source ~/source/bash_scripts/mplayer_eq.sh

# Custom Bash prompt
#source ~/source/bash_scripts/bashPrompt.sh

# setV
# git clone https://gitlab.com/psachin/setv ~/source/setv
source ~/source/setv/virtual.sh

# set emacs as default editor
export EDITOR="emacsclient -t"

# for xterm(it uses vim key-bindings by default)
set -o emacs

# History
export HISTTIMEFORMAT="%F %T "
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # many commands in ongoing session memory
export HISTFILESIZE=100000               # many lines in .bash_history
shopt -s histappend                      # append to history, don't overwrite it
# --------------------
